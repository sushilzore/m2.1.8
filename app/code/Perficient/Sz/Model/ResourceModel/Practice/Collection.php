<?php

namespace Perficient\Sz\Model\ResourceModel\Practice;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Perficient\Sz\Model\Practice', 'Perficient\Sz\Model\ResourceModel\Practice');
    }
}