<?php

namespace Perficient\Sz\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\MailException;

class Practice extends \Magento\Framework\Model\AbstractModel
{
    protected $_eventPrefix = 'perficient_sz';

    protected function _construct()
    {
        $this->_init('Perficient\Sz\Model\ResourceModel\Practice');
    }
}