<?php

namespace Perficient\Sz\Block;
use Magento\Framework\UrlInterface;

class First extends \Magento\Framework\View\Element\Template
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        UrlInterface $urlBuilder,
        array $data = []
    )
    {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('first.phtml');
    }

    public function getPostActionUrl()
    {
        return $this->urlBuilder->getUrl('practice/index/post');
    }

    public function isAutocompleteDisabled()
    {
        return true;
    }

}