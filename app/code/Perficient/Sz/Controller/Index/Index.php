<?php

namespace Perficient\Sz\Controller\Index;

use Braintree\Exception;
use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;

    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        $result = null;
        try{
            $result = $this->_resultPageFactory->create();
        } catch (Exception $e) {

        }
        return $result;
    }
}