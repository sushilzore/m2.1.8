<?php

namespace Perficient\Sz\Controller\Index;

use Braintree\Exception;
use Magento\Framework\App\Action\Context;

class Post extends \Magento\Framework\App\Action\Action
{
    /**
     * @param Context $context
     * @param Session $customerSession
     * @param AccountManagementInterface $customerAccountManagement
     * @param Escaper $escaper
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPost('practice');
        try {
            $model = $this->_objectManager->create('Perficient\Sz\Model\Practice');
            $model->setData($data);
            $model->save();
            $this->messageManager->addSuccessMessage($this->getSuccessMessage());

        } catch (Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        return $resultRedirect->setPath('*/*/');
    }


    /**
     * Retrieve success message
     *
     * @param string $email
     * @return \Magento\Framework\Phrase
     */
    protected function getSuccessMessage()
    {
        return __(
            'The test practice data has been stored successfully.'
        );
    }
}