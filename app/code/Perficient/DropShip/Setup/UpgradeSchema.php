<?php
namespace Perficient\DropShip\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
/**
 * Upgrade the Cms module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '0.0.2', '<')) {
            $this->addFullTextIndex($setup);
        }
    }

    /**
     * Add meta title
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    protected function addFullTextIndex(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addIndex(
            $setup->getTable('prf_warehouse_manager'),
            $setup->getIdxName(
                ['title', 'description', 'first_name', 'last_name', 'username'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['title', 'description', 'first_name', 'last_name', 'username'],
            AdapterInterface::INDEX_TYPE_FULLTEXT
        );
        return $this;
    }
}
