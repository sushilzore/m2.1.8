<?php

namespace Perficient\DropShip\Model\ResourceModel\Warehouse;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Perficient\DropShip\Model\Warehouse', 'Perficient\DropShip\Model\ResourceModel\Warehouse');
    }
}