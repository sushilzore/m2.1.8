<?php

namespace Perficient\DropShip\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\MailException;

class Warehouse extends \Magento\Framework\Model\AbstractModel
{
    protected $_eventPrefix = 'dropship_warehouse';

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    protected function _construct()
    {
        $this->_init('Perficient\DropShip\Model\ResourceModel\Warehouse');
    }
}