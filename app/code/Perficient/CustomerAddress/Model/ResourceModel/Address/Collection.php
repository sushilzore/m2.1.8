<?php

namespace Perficient\CustomerAddress\Model\ResourceModel\Address;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Perficient\CustomerAddress\Model\Address', 'Perficient\CustomerAddress\Model\ResourceModel\Address');
    }

    public function setCustomerFilter($customer)
    {
        if (is_array($customer)) {
            $this->addFieldToFilter('parent_id', ['in' => $customer]);
        } elseif ($customer->getId()) {
            $this->addFieldToFilter('parent_id', $customer->getId());
        } else {
            $this->addFieldToFilter('parent_id', '-1');
        }
        return $this;
    }

    public function addAttributeToSelect($attribute)
    {
        $this->addFieldToSelect($attribute);
        return $this;
    }
}