<?php

namespace Perficient\CustomerAddress\Model\ResourceModel;

class Address extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('erp_customer_address', 'id');
    }

}