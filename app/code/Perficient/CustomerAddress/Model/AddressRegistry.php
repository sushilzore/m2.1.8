<?php

namespace Perficient\CustomerAddress\Model;

class AddressRegistry extends \Magento\Customer\Model\AddressRegistry
{
    public function __construct(AddressFactory $addressFactory)
    {
        $this->addressFactory = $addressFactory;
    }

    public function retrieve($addressId)
    {
        if (isset($this->registry[$addressId])) {
            return $this->registry[$addressId];
        }
        $address = $this->addressFactory->create();
        $address->load($addressId);;
        if (!$address->getId()) {
            throw NoSuchEntityException::singleField('addressId', $addressId);
        }
        $this->registry[$addressId] = $address;
        return $address;
    }

}